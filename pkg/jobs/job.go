package jobs

const (
	CONTROL_STATUS_CODE_UNKNOWN    ControlStatusCodeType = 0
	CONTROL_STATUS_CODE_STARTED    ControlStatusCodeType = 1
	CONTROL_STATUS_CODE_RUNNING    ControlStatusCodeType = 2
	CONTROL_STATUS_CODE_RETRYING   ControlStatusCodeType = 3
	CONTROL_STATUS_CODE_INCOMPLETE ControlStatusCodeType = 4
	CONTROL_STATUS_CODE_FINISHED   ControlStatusCodeType = 5

	CONTROL_STATUS_UNKNOWN    ControlStatusType = "UNKNOWN"
	CONTROL_STATUS_STARTED    ControlStatusType = "STARTED"
	CONTROL_STATUS_RUNNING    ControlStatusType = "RUNNING"
	CONTROL_STATUS_RETRYING   ControlStatusType = "RETRYING"
	CONTROL_STATUS_INCOMPLETE ControlStatusType = "INCOMPLETE"
	CONTROL_STATUS_FINISHED   ControlStatusType = "FINISHED"

	ERROR_SEVERITY_CODE_UNKNOWN ErrorSeverityCodeType = 0
	ERROR_SEVERITY_CODE_DEBUG   ErrorSeverityCodeType = 1
	ERROR_SEVERITY_CODE_INFO    ErrorSeverityCodeType = 2
	ERROR_SEVERITY_CODE_WARNING ErrorSeverityCodeType = 3
	ERROR_SEVERITY_CODE_ERROR   ErrorSeverityCodeType = 4
	ERROR_SEVERITY_CODE_PANIC   ErrorSeverityCodeType = 5
	ERROR_SEVERITY_CODE_FATAL   ErrorSeverityCodeType = 6

	ERROR_SEVERITY_UNKNOWN ErrorSeverityType = "UNKNOWN"
	ERROR_SEVERITY_DEBUG   ErrorSeverityType = "DEBUG"
	ERROR_SEVERITY_INFO    ErrorSeverityType = "INFO"
	ERROR_SEVERITY_WARNING ErrorSeverityType = "WARNING"
	ERROR_SEVERITY_ERROR   ErrorSeverityType = "ERROR"
	ERROR_SEVERITY_PANIC   ErrorSeverityType = "PANIC"
	ERROR_SEVERITY_FATAL   ErrorSeverityType = "FATAL"
)

var (
	ControlStatus = map[ControlStatusCodeType]ControlStatusType{
		CONTROL_STATUS_CODE_UNKNOWN:    CONTROL_STATUS_UNKNOWN,
		CONTROL_STATUS_CODE_STARTED:    CONTROL_STATUS_STARTED,
		CONTROL_STATUS_CODE_RUNNING:    CONTROL_STATUS_RUNNING,
		CONTROL_STATUS_CODE_RETRYING:   CONTROL_STATUS_RETRYING,
		CONTROL_STATUS_CODE_INCOMPLETE: CONTROL_STATUS_INCOMPLETE,
		CONTROL_STATUS_CODE_FINISHED:   CONTROL_STATUS_FINISHED,
	}
	ControlStatusCode = map[ControlStatusType]ControlStatusCodeType{
		CONTROL_STATUS_UNKNOWN:    CONTROL_STATUS_CODE_UNKNOWN,
		CONTROL_STATUS_STARTED:    CONTROL_STATUS_CODE_STARTED,
		CONTROL_STATUS_RUNNING:    CONTROL_STATUS_CODE_RUNNING,
		CONTROL_STATUS_RETRYING:   CONTROL_STATUS_CODE_RETRYING,
		CONTROL_STATUS_INCOMPLETE: CONTROL_STATUS_CODE_INCOMPLETE,
		CONTROL_STATUS_FINISHED:   CONTROL_STATUS_CODE_FINISHED,
	}

	ErrorSeverity = map[ErrorSeverityCodeType]ErrorSeverityType{
		ERROR_SEVERITY_CODE_UNKNOWN: ERROR_SEVERITY_UNKNOWN,
		ERROR_SEVERITY_CODE_DEBUG:   ERROR_SEVERITY_DEBUG,
		ERROR_SEVERITY_CODE_INFO:    ERROR_SEVERITY_INFO,
		ERROR_SEVERITY_CODE_WARNING: ERROR_SEVERITY_WARNING,
		ERROR_SEVERITY_CODE_ERROR:   ERROR_SEVERITY_ERROR,
		ERROR_SEVERITY_CODE_PANIC:   ERROR_SEVERITY_PANIC,
		ERROR_SEVERITY_CODE_FATAL:   ERROR_SEVERITY_FATAL,
	}
	ErrorSeverityCode = map[ErrorSeverityType]ErrorSeverityCodeType{
		ERROR_SEVERITY_UNKNOWN: ERROR_SEVERITY_CODE_UNKNOWN,
		ERROR_SEVERITY_DEBUG:   ERROR_SEVERITY_CODE_DEBUG,
		ERROR_SEVERITY_INFO:    ERROR_SEVERITY_CODE_INFO,
		ERROR_SEVERITY_WARNING: ERROR_SEVERITY_CODE_WARNING,
		ERROR_SEVERITY_ERROR:   ERROR_SEVERITY_CODE_ERROR,
		ERROR_SEVERITY_PANIC:   ERROR_SEVERITY_CODE_PANIC,
		ERROR_SEVERITY_FATAL:   ERROR_SEVERITY_CODE_FATAL,
	}
)

type ControlStatusType string
type ControlStatusCodeType int

type ErrorSeverityType string
type ErrorSeverityCodeType int

type Job struct {
	Id          string  `json:"id"`
	ReferenceId string  `json:"reference_id"`
	Type        string  `json:"type"`
	Plan        JobPlan `json:"plan"`
}

type JobPlan struct {
	Name       string `json:"name"`
	UseDefault bool   `json:"useDefault"`
}

type URLPayload struct {
	URL  string            `json:"url"`
	Meta map[string]string `json:"meta"`
}

type FilePayload struct {
	Contents []byte            `json:"contents"`
	Name     string            `json:"name"`
	Meta     map[string]string `json:"meta"`
}

type Message struct {
	Id          string      `json:"id"`
	ReferenceId string      `json:"reference_id"`
	Payload     interface{} `json:"payload"`
}

type Result struct {
	Output string            `json:"output"`
	Meta   map[string]string `json:"meta"`
}

type Control struct {
	Status  ControlStatusCodeType `json:"status"`
	Message string                `json:"message"`
}

type Error struct {
	Severity ErrorSeverityCodeType `json:"severity"`
	Code     string                `json:"code"`
	Message  string                `json:"message"`
}
